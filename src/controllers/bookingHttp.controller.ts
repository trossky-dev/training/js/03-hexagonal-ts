import saveBooking from "../core/interactors";
import {Request, Response} from "express";

const bookingController = async (request: Request, response: Response) => {
  const {body}= request;
  const {passanger, trip} = body;

  const ticket = await saveBooking(passanger,trip);
  response.json(ticket);
}


export default bookingController