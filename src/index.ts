import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import bookingController from './controllers/bookingHttp.controller';


dotenv.config();
const PORT=8080;

const app= express();

app.use(bodyParser.json());

app.post('/booking', bookingController)

app.listen  (PORT,()=>{
  console.log(`Server starting in ${PORT}`);
  
})