import Ticket from "core/entities/Ticket";
import Trip from "core/entities/Trip";
import TripRepository from "core/repositories/trip.repository";
import NotifierRepository from "core/repositories/notifier.repository";
import Passanger from "../entities/Passanger";
  
const saveBooking=(tripRepository:TripRepository, notifierRepository: NotifierRepository)=> async (
  passanger: Passanger, tripId: string ): Promise<Ticket> =>{

  // Get trip by Id
  const trip:Trip= await tripRepository.getById(tripId);

  console.log(passanger);
  

  // Create Ticket 
  const ticket:Ticket = {
    tripId: trip.id,
    tripName: trip.name,
    arrivalTime:trip.arrivalTime,
    departureTime:trip.departureTime,
    passengerName:passanger.name,
    passengerSurname:passanger.surname 
  }

  // Notify Passanger about 
  notifierRepository.notify(ticket,passanger.email); 

  // Return ticket
  return ticket;  
}

export default saveBooking;