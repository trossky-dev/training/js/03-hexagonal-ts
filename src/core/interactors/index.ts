import EmailNotifier from "../../datasource/emailNotifier";
import TripMongo from "../../datasource/tripMongo.datasource";
import saveBooking from "./saveBooking.interactor";

const tripRepository= new TripMongo();
const notifierRepository= new EmailNotifier();

export default saveBooking(tripRepository, notifierRepository);

