import Trip from "core/entities/Trip";

export default interface TripRepository{
  getById(id: string): Promise<Trip>;
  
   
}