import Trip from "core/entities/Trip";
import TripRepository from "core/repositories/trip.repository";
import MongoClient from 'mongodb';

 export default class TripMongo implements TripRepository{
   
  /**
   * getById
   */
  public async getById( id:string): Promise<Trip> {

    const collection= await this.getCollection();
    const trip = await collection.findOne({id}) 
    return trip;
    
  }
  
     /// COllection per request
     private async getCollection() {
      const url = 'mongodb://192.168.1.12:27017';
      const client = await MongoClient.connect(url, { useUnifiedTopology: true });
  
      const db = client.db('ticketing');
      return db.collection('trips');
    }
  
 }


